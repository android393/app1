package com.example.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.widget.TextView
import org.w3c.dom.Text

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        val msg = intent.getStringExtra(EXTRA_MESSAGE)
        val textView = findViewById<TextView>(R.id.input_text).apply {
            text = msg
        }
    }
}