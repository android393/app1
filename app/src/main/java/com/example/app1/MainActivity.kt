package com.example.app1

import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import com.example.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val helloName: TextView = findViewById(R.id.text_name)
        val helloId: TextView = findViewById(R.id.text_id_stu)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnHello.setOnClickListener{
            Log.d(TAG, ""+helloName.text)
            Log.d(TAG, ""+helloId.text)
            setContentView(R.layout.activity_hello)
            val msg = helloName.text
            val intent = Intent(this, HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, msg)
            }
            startActivity(intent)
        }

    }

    companion object{
        private const val TAG = "MainActivity"
    }
}